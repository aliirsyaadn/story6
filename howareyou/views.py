from django.shortcuts import render, redirect
from .models import Status
from .forms import StatusForm

def status_create(request):
    if request.method == 'POST':
        form = StatusForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('howareyou:status_create')
    else:
        form = StatusForm()
    data = Status.objects.all()
    return render(request, 'howareyou.html', {'form': form, 'data': data})

