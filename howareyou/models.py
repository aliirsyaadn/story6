from django.db import models
from django.utils import timezone


# Create your models here.
class Status(models.Model):
    status = models.CharField(null=False, blank=False, max_length = 301)
    date = models.DateTimeField(default = timezone.now)
