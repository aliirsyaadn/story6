from howareyou.forms import StatusForm
from howareyou.models import Status
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.urls import reverse
import time



class Story6FunctionalTest(StaticLiveServerTestCase):
    def setUp(self):

        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Story6FunctionalTest, self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(Story6FunctionalTest, self).tearDown()


    def test_title_element_in_project(self):
        self.browser.get(self.live_server_url)

        title = self.browser.find_element_by_tag_name('h1')
        self.assertEquals(title.text, "Hello, How Are You ?")
        time.sleep(10)

    def test_status_form(self):
        self.browser.get(self.live_server_url)

        form = self.browser.find_element_by_id('status')
        submit = self.browser.find_element_by_id('submit')

        form.send_keys("Baik :)")
        submit.click()

        time.sleep(10)
        self.assertIn("Baik :)", self.browser.page_source)
        