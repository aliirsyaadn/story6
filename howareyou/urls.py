from django.urls import path
from .views import status_create

app_name = 'howareyou'

urlpatterns = [
    path('', status_create, name='status_create'),
]