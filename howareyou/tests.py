from django.test import TestCase, Client
from django.urls import resolve

from .views import status_create
from .models import Status
from .forms import StatusForm


# Unit Test
class Story6Test(TestCase):
    def test_story_6_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)

    def test_story_6_using_to_do_list_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'howareyou.html')

    def test_story_6_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, status_create)

    def test_story_6_form_is_valid(self):
        form = StatusForm(data={})

        self.assertFalse(form.is_valid())
        self.assertEquals(len(form.errors), 1)

        form = StatusForm(data={'status':'Baik:)'})
        self.assertTrue(form.is_valid())

    def test_story_6_models(self):
        self.assertFalse(Status.objects.all().exists())
        
        status = Status.objects.create(status = "Kurang Baik :(")

        self.assertTrue(Status.objects.all().count(), 1)
        self.assertTrue(Status.objects.filter(status= "Kurang Baik :(").exists())
        self.assertTrue(status.status, "Kurang Baik :(")