$(document).ready(function(){
    $('.panels').click(function(e) {
       e.preventDefault();
  
     var $this = $(this);
  
     if ($this.next().hasClass('show')) {
         $this.next().removeClass('show');
         $this.next().slideUp(350);
     } else {
         $this.parent().parent().find('li .inside').removeClass('show');
         $this.parent().parent().find('li .inside').slideUp(350);
         $this.next().toggleClass('show');
         $this.next().slideToggle(350);
     }
   });
});
  
$("#switch").on("click", () => {
    if($("#switch").prop("checked")) {
        $("body").removeClass("white");  
        $("body").addClass("dark");
        $("h1").addClass("white-font");
        $(".inside").addClass("white-font");
        $("label").addClass("white-font");
        $("label").text("Dark Mode");
    } else {
        $("body").removeClass("dark");
        $("body").addClass("white");
        $("h1").removeClass("white-font");
        $(".inside").removeClass("white-font");
        $("label").removeClass("white-font");
        $("label").text("White Mode");
    }
});