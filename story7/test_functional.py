from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.urls import reverse
import time



class Story7FunctionalTest(StaticLiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Story7FunctionalTest, self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(Story7FunctionalTest, self).tearDown()


    def test_title_element_in_project(self):
        self.browser.get(self.live_server_url + "/story7/")

        title = self.browser.find_element_by_tag_name('h1')
        self.assertEquals(title.text, "My Profile")
        time.sleep(10)

    def test_accordion(self):
        self.browser.get(self.live_server_url + "/story7/")

        accordion = self.browser.find_element_by_class_name('test-accordion')

        accordion.click()

        time.sleep(10)
        self.assertIn("1st Place in JKTC Pencak Silat Class E", self.browser.page_source)
        