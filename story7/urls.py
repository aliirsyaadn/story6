from django.urls import path
from .views import myprofile

app_name = 'story7'

urlpatterns = [
    path('', myprofile, name='myprofile'),
]