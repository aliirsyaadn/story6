from django.test import TestCase, Client
from django.urls import resolve

from .views import myprofile



# Unit Test
class Story7Test(TestCase):
    def test_story_7_url_is_exist(self):
        response = Client().get('/story7/')
        self.assertEqual(response.status_code,200)

    def test_story_7_using_to_do_list_template(self):
        response = Client().get('/story7/')
        self.assertTemplateUsed(response, 'myprofile.html')

    def test_story_6_using_index_func(self):
        found = resolve('/story7/')
        self.assertEqual(found.func, myprofile)
