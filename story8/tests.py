from django.test import TestCase, Client
from django.urls import resolve

from .views import bookSearch



# Unit Test
class Story8Test(TestCase):
    def test_story_8_url_is_exist(self):
        response = Client().get('/story8/')
        self.assertEqual(response.status_code,200)

    def test_story_8_using_to_do_list_template(self):
        response = Client().get('/story8/')
        self.assertTemplateUsed(response, 'bookSearch.html')

    def test_story_8_using_index_func(self):
        found = resolve('/story8/')
        self.assertEqual(found.func, bookSearch)
