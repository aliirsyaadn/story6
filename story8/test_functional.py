from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.urls import reverse
import time



class Story8FunctionalTest(StaticLiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Story8FunctionalTest, self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(Story8FunctionalTest, self).tearDown()


    def test_title_element_in_project(self):
        self.browser.get(self.live_server_url + "/story8/")

        title = self.browser.find_element_by_tag_name('h1')
        self.assertEquals(title.text, "Book Search")
        time.sleep(10)

    def test_search(self):
        self.browser.get(self.live_server_url + "/story8/")

        form = self.browser.find_element_by_id("key")
        button = self.browser.find_element_by_id("search")

        form.send_keys("Sapiens")
        button.click()

        time.sleep(10)
        self.assertIn("Author : Yuval Noah Harari", self.browser.page_source)
        