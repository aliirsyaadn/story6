from django.urls import path
from .views import bookSearch

app_name = 'story8'

urlpatterns = [
    path('', bookSearch, name='bookSearch'),
]