$(document).ready(function(){   
    var imageDefault = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAANlBMVEXz9Pa5vsq2u8jN0dnV2N/o6u7FydPi5Onw8fS+ws3f4ee6v8v29/jY2+Hu7/Ly9PbJztbQ1dxJagBAAAAC60lEQVR4nO3b2ZaCMBREUQbDJOP//2wbEGVIFCHKTa+zH7uVRVmBBJQgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMCpdOzvQQqaq2KmuSrOzQ02lSeRem8rpsQq/ozg72Kj4UkAxEev8awnzs7P1yiIadsfpQXjfZCHhUCzbfmeurdNz6bDRsBWRsB+k0cXxdHjpa0wkTBn3hKnjzRZyEgYk3IeEv2RKWCt1cN9EJ0zjfm7Mq/rAVgUnbLpwnK/zA2tnuQmzJHquuqJq91blJuwmAW8rHbV3q2ITFrOAt7Xz3l2UmrBMlpcHe9fOUhOqRYVhFO/cqtSEy0H6bh/tJ1uhCctqlTB/NSnG9pOt1ISXjxLq825laVFowo9GaRPrF9talJqw3n6macaZ09yi1ISG2cLyriwePwxzi1ITru4s2naxma59TC2KTRjE83FqmQ6yeDaUDS3KTRhMV96h5TTSLD4HQ4uCE9bxePUU5pYL/3mD5o9CcMKgTONc39NNLrV5iK4aNLUoOWHQ38RQtW3nsm6db92i8ISvGBtct+hvwqyzBFxE9DehrcHlQPU1YWNvcNGirwlfNThv0ZOE9eJG1OsGZy36kVBdczU9e7RvAz5b9CFhqfIwSp4XwG+OwUWLPiRUV/33Z4tbGtTvGK635CfUDfb/SO5rt20N9t8m65fLT9g3GD5abDY2qC+lvEg4NjhEvLW4tUFvEj4a7OXq3TzoW8Jpg0PEzfk8SThv8EMeJFw1+O8SHmrQg4QHG/Qg4cEGxSc83KD4hIcblJ6w3L508TXh+vtDEpLw3GwDEpKQhOdznVD2fRr9tdpRw/1HqQndIeEvkXCXUlDC+1NBndsnge/fwyVnp9PGH3p95dm1WMKza4/fI37j+UPXR/c+2X9/hjQI0uO3LsyuMioM9A8Sjy/W1iIhY7Sn2tzpUahdWyXiNDNSxcWtSlCBAAAAAAAAAAAAAAAAAAAAAAAAAAAAwCn+AEXGNosxDBhFAAAAAElFTkSuQmCC";

    $.ajax({
        url: "https://www.googleapis.com/books/v1/volumes?q=hacker",
        dataType: "json",
        success: (data) => {
            for(var i = 0; i < data.items.length; i++){
                var thumbnail, authors;
                if(data.items[i].volumeInfo.imageLinks != null){
                    thumbnail = data.items[i].volumeInfo.imageLinks.thumbnail;
                } else{
                    thumbnail = imageDefault;
                }

                if(data.items[i].volumeInfo.authors != null){
                    authors = data.items[i].volumeInfo.authors;
                } else{
                    authors = "Unknown";
                }

                $("#content").append('<div class="card col-md-4 mt-4 ml-auto mr-auto" style="width: 18rem;"><img style="width:200px; height:300px;" src="'+ thumbnail +'" class="card-img-top m-auto" alt="..."><div class="card-body"><h5 class="card-title">'+ data.items[i].volumeInfo.title +'</h5><p class="card-text"> Author : '+ authors +'</p><a target="_blank" href="'+data.items[i].volumeInfo.infoLink +'" class="btn btn-warning mx-auto">More Info</a><a target="_blank" href="'+ data.items[i].volumeInfo.previewLink +'" class="btn btn-success mx-auto">Book Preview</a></div></div>')
            }
        },
        type: 'GET'

    });

    $("#search").click(() => {
        var key = $("#key").val();
        $("#content").text('');

        $.ajax({
            url: "https://www.googleapis.com/books/v1/volumes?q="+key,
            dataType: "json",
            success: (data) => {
                for(var i = 0; i < data.items.length; i++){
                    var thumbnail, authors;
                    if(data.items[i].volumeInfo.imageLinks != null){
                        thumbnail = data.items[i].volumeInfo.imageLinks.thumbnail;
                    } else{
                        thumbnail = imageDefault;
                    }

                    if(data.items[i].volumeInfo.authors != null){
                        authors = data.items[i].volumeInfo.authors;
                    } else{
                        authors = "Unknown";
                    }

                    $("#content").append('<div class="card col-md-4 mt-4 ml-auto mr-auto" style="width: 18rem;"><img style="width:200px; height:300px;" src="'+ thumbnail +'" class="card-img-top m-auto" alt="..."><div class="card-body"><h5 class="card-title">'+ data.items[i].volumeInfo.title +'</h5><p class="card-text"> Author : '+ authors +'</p><a target="_blank" href="'+data.items[i].volumeInfo.infoLink +'" class="btn btn-warning mx-auto">More Info</a><a target="_blank" href="'+ data.items[i].volumeInfo.previewLink +'" class="btn btn-success mx-auto">Book Preview</a></div></div>')
                }
            },
            type: 'GET'

        });

         
        

        $("#key").val('');
    });
});
  