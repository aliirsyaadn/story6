from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('howareyou.urls')),
    path('story7/', include('story7.urls')),
    path('story8/', include('story8.urls')),
]
